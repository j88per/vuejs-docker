FROM ubuntu

RUN apt-get update
RUN apt-get -y install python-software-properties git build-essential libc6-dev software-properties-common curl nginx
RUN curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get update
RUN apt-get install nodejs

WORKDIR /var/www/html

ADD ./src /var/www/html
RUN cd /var/www/html
RUN npm install

EXPOSE 80

STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]
