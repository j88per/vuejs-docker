// src/main.js
import Vue from 'vue';
import App from './App.vue';

// main
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
});
