var path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const PATHS = {
    app: path.join(__dirname, './'),
    sass: path.join(__dirname, './assets/sass'),
    build: path.join(__dirname, 'dist')
}

// Main Settings config

module.exports = {
    entry: './main.js',
    output: {
        filename: 'build.js',
        path: PATHS.build
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
            node: 'node_modules'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }, {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader', 
                    use: ['css-loader', 'sass-loader']
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('dist.css')
    ]
};
