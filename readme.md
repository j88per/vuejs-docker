# README #

This is my exploration into Vue.js development, on docker.  It is NOT based on standards around Vue.js development.
I prefer to take the hard road when learning something new, it gives me a better perspective on what that tool is, 
what it's capable of doing, and how it does it's thing.  
I am intentionally NOT following Vue.js guidelines and forging my own path.

### What is this repository for? ###

* Pure Vue.js
* do I need one?  it's just me

### How do I get set up? ###

* Summary of set up

Setup is pretty straightforward for now, git clone to clone the repo, docker build -t <tag name> . to build an image and
* To run the image
docker run -i -t -p <your preferred port>:80 -v /home/user/project/src:/var/www/html -e VIRTUAL_HOST=vuejs.dev --name vuejs <image name> nginx -g daemon off

Your preferred port is the port to access.  I am using jWilder/nginx-proxy to handle half a dozen web apps at any given time in dev, so 
jwilder's proxy config allows me to keep them all running at the same time.

-v is the volume to mount:target, this is currently in flux as I'm tweaking the various configurations to see which I like best.  Either sharing just
a pure src/dist folder or a bigger picture.

-e VIRTUAL_HOST is used by the nginx-proxy to simulate host based routing

* Configuration
Currently there is no configuration.  Eventually I plan to have an nginx configuration file for improved vhost control.

* Dependencies
All system dependencies are installed

* Database configuration
Not Applicable - This doesn't use a db unless you configure a need for it.

* How to run tests
Not done yet

* Deployment instructions
Not done yet

### Contribution guidelines ###
Wasn't planning on anything here, but if someone chooses to fork and submit a PR, I'd be thrilled.

### Who do I talk to? ###
* Andrew <j88per@gmail.com>
